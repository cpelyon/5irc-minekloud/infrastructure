# Flux 2

This folder contain ressources managed by Flux 2 in a GitOps way

Learn more: [fluxcd/flux2](https://github.com/fluxcd/flux2)


# Bootstrap

Flux was setup manually using a GitLab Access Token and:

```
flux bootstrap gitlab --owner=cpelyon/5irc-minekloud --repository=infrastructure --branch=master --path=flux
```


TO:DO move bootstraping in CI steps like Terraform [docs](https://toolkit.fluxcd.io/guides/installation/#bootstrap-with-terraform)