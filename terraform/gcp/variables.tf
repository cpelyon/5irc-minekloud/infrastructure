variable "location" {
  type        = string
  description = "The location"
  default     = "europe-west1-c"
}

variable "pool_size" {
  type        = number
  default  = 4    
}
