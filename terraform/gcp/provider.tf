provider "google" {
  credentials = file("./service-account.json")
  project     = "minekloud-k8s"
  region      = "us-central1-c"
}
